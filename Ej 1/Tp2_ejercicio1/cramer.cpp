#include "cramer.h"
#include <iostream>

using namespace std;

Cramer::Cramer()
{
}

void Cramer::calcular(Matriz mat)
{
    if((mat.getCantC()-1) != mat.getCantF()){ //Comprobamos que la matriz sea cuadrada
        cout << "Error: la matriz no es cuadrada" << endl;

    }
    else if((mat.getCantC()-1) > 3 && mat.getCantF() > 3){ //Comprobamos que la matriz no sea de orden mayor a 3
        cout << "Error: la matriz es de orden mayor a 3" << endl;

    }
    else
    {
        setAux(mat);

        if(mat.getCantF() == 2)//Si la matriz es 2x2 entraremos en el if
            cramer2();

        else//Si la matriz es 3x3 entraremos en el else
            cramer3();
    }
}




void Cramer::cramer2()
{
    determinante();//Calculamos el determinante de la matriz

    if (det == 0){ //Si el determinante nos da 0
        cout << "Error: La matriz tiene infinitas soluciones o no tiene soluciones";

    }
    else
    {


        //Creamos las matrices aux1 y aux2

        //Creamos las filas

        float **aux1 = new float*[m];
        float **aux2 = new float*[m];

        for(int i=0; i<m; i++){ //Creamos las columnas
            aux1[i] = new float[n-1];
            aux2[i] = new float[n-1];
        }

        //Asignamos a cada matriz los valores correspondientes
        //Para la matriz aux1
        for(int i=0; i<m; i++)
        {

            aux1[i][0] = aux[i][2];//Se asignan los valores de la primera columna

            aux1[i][1] = aux[i][1];//Se asignan los valores de la segunda columna
        }

        //Para la matriz aux2
        for(int i=0; i<m; i++)
        {
            aux2[i][0] = aux[i][0]; //Se asignan los valores de la primera columna

            aux2[i][1] = aux[i][2];; //Se asignan los valores de la segunda columna
        }
        //Calculamos los determinantes de aux1 y aux2
        float det1, det2;

        det1=determinante(aux1,m);
        det2=determinante(aux2,m);


        //Creamos el vector que devolverá las soluciones y le cargamos los valores correspondientes
        //        float *vect;
        this-> solucion = new float[m];
        this-> solucion[0] = det1 / det;
        this-> solucion[1] = det2 / det;
        //                for(int i=0;i<m;i++)
        //                    cout <<endl<< "solucion["<<i<<"]: "<<solucion[i]<<endl;

    }
}



void Cramer::cramer3()
{
    determinante();//Calculamos el determinante de la matriz
    if (det == 0){ //Si el determinante nos da 0
        cout << "Error: La matriz tiene infinitas soluciones o no tiene soluciones";

    }
    else
    {


        //Creamos las matrices aux1, aux2 y aux3

        //Creamos las filas

        float **aux1 = new float*[m];
        float **aux2 = new float*[m];
        float **aux3 = new float*[m];

        for(int i=0; i<m; i++){ //Creamos las columnas
            aux1[i] = new float[n-1];
            aux2[i] = new float[n-1];
            aux3[i] = new float[n-1];
        }

        //Asignamos a cada matriz los valores correspondientes
        //Para la matriz aux1
        for(int i=0; i<m; i++){
            aux1[i][0] = aux[i][3]; //Se asignan los valores de la primera columna
            aux1[i][1] = aux[i][1]; //Se asignan los valores de la segunda columna
            aux1[i][2] = aux[i][2]; //Se asignan los valores de la tercera columna
        }

        //Para la matriz aux2
        for(int i=0; i<m; i++){
            aux2[i][0] = aux[i][0]; //Se asignan los valores de la primera columna
            aux2[i][1] = aux[i][3]; //Se asignan los valores de la segunda columna
            aux2[i][2] = aux[i][2]; //Se asignan los valores de la tercera columna
        }
        //Para la matriz aux3
        for(int i=0; i<m; i++){
            aux3[i][0] = aux[i][0]; //Se asignan los valores de la primera columna
            aux3[i][1] = aux[i][1]; //Se asignan los valores de la segunda columna
            aux3[i][2] = aux[i][3]; //Se asignan los valores de la tercera columna
        }
        //Calculamos los determinantes de aux1, aux2 y aux3
        float det1, det2, det3;

        det1 = determinante(aux1,m);
        det2 = determinante(aux2,m);
        det3 = determinante(aux3,m);

        //Creamos el vector que devolverá las soluciones y le cargamos los valores correspondientes

        this-> solucion = new float[m];
        this-> solucion[0] = det1 / det;
        this-> solucion[1] = det2 / det;
        this-> solucion[2] = det3 / det;

        //                for(int i=0;i<m;i++)
        //                    cout <<endl<< "solucion["<<i<<"]: "<<solucion[i]<<endl;

    }
}



void Cramer::determinante()
{
    if(m == 2)
    {
        this->det = aux[0][0] * aux[1][1] - aux[1][0] * aux[0][1];
    }
    if(m==3)
    {
        this->det = (aux[0][0] * aux[1][1] * aux[2][2] + aux[1][0] * aux[2][1] * aux[0][2]
                +aux[1][2] * aux[0][1] * aux[2][0]) - (aux[0][2] * aux[1][1] * aux[2][0]
                +aux[1][2] * aux[2][1] * aux[0][0] + aux[2][2] * aux[0][1] * aux[1][0]);
    }


}

float Cramer::determinante(float **matriz, int elementos)
{
    float determinante=0;
    if(elementos==2)
    {
        determinante = matriz[0][0] * matriz[1][1] - matriz[1][0] * matriz[0][1];
    }
    else
    {
        determinante =  (matriz[0][0] * matriz[1][1] * matriz[2][2] + matriz[1][0] * matriz[2][1] * matriz[0][2]
                +matriz[1][2] * matriz[0][1] * matriz[2][0]) - (matriz[0][2] * matriz[1][1] * matriz[2][0]
                +matriz[1][2] * matriz[2][1] * matriz[0][0] + matriz[2][2] * matriz[0][1] * matriz[1][0]);
    }
    return determinante;
}

void Cramer::mostrar(float**matriz,int dimension)
{
    cout <<"mostrando" <<endl;
    for (int i=0;i<dimension;i++)
    {

        for(int j=0;j<dimension;j++)
        {
            cout <<" " <<matriz[i][j] << " ";
        }
        cout << endl;
    }
}


Cramer::~Cramer(){

}
