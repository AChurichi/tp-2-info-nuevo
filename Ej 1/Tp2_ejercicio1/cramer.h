#ifndef CRAMER_H
#define CRAMER_H
#include "metodo.h"


class Cramer:public Metodo
{
protected:
    float det;
    void determinante();
    float determinante(float **,int);
    void mostrar(float**,int);
    void cramer2();
    void cramer3();

public:
    Cramer();
    void calcular(Matriz);

    ~Cramer();
};

#endif // CRAMER_H
