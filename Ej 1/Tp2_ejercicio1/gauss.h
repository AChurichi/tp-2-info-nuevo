#ifndef GAUSS_H
#define GAUSS_H
#include "metodo.h"

#include <iostream>

using namespace std;

class Gauss:public Metodo
{
protected:
    void triSup();//triangular superior
    void sustitucionRegresiva();
    float sumatoria(int);
    void intercambiarRenglon(int r1, int r2);

public:
    Gauss();
    virtual void calcular(Matriz);
};

#endif // GAUSS_H
