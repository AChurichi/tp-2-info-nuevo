TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    matriz.cpp \
    metodo.cpp \
    cramer.cpp \
    gauss.cpp

HEADERS += \
    matriz.h \
    metodo.h \
    cramer.h \
    gauss.h
