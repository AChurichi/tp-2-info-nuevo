#ifndef METODO_H
#define METODO_H
#include "matriz.h"
#include <iostream>

class Metodo
{

protected:

    float *solucion;
    float **aux;
    int m;
    int n;
public:
    Metodo();
    virtual void calcular(Matriz)=0;
    float *getSolucion();
    void setAux(Matriz);
    void mostrar();

};

#endif // METODO_H
