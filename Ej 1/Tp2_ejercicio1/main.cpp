#include <iostream>
#include "matriz.h"
#include "gauss.h"
#include "cramer.h"
#include <cstring>

using namespace std;

int main(){
    FILE *input;
    char nombreArch[100];
    cout << "Como se llama el archivo?" << endl;
    scanf("%[^\n]%*c",nombreArch);
    input = fopen(nombreArch, "r");
    if(!input){
        cout << "Error archivo no encontrado" << endl;
        return -1;
    }
    char fila[100];
    while(!feof(input)){
        if(fgets(fila, 100, input) != NULL) //si no se hace esto te copia dos veces el ultimo renglon
            cout << fila << endl;
    }
    fclose(input);
    return 0;
}


/*int main()
{
    Matriz *m;
    Metodo *prueba;

    //        int a=2;
    //        int b=3;

    int a=3;
    int b=4;

    //    int a=4;
    //    int b=5;

    //    int a=5;
    //    int b=6;
    cout << "Vamos a probar" << endl;
    //        float v[2][3]={{1,4,3},{4,2,9}};
    float v[3][4]={{1,2,5,3},{4,5,6,2},{7,8,9,5}};
    //    float v[4][5]={{1,3,4,6,2},{2,6,7,1,4},{3,5,2,1,6},{2,8,3,5,8}};
    //    float v[5][6]={{2,3,4,6,2,3},{2,6,7,1,4,6},{3,5,9,4,6,7},{2,8,0,3,8,1},{1,3,4,6,2,9}};
    m = new Matriz(a,b);
    for(int i=0;i<a;i++)
    {
        for(int j=0;j<b;j++)
        {
            m->setVal(v[i][j],i,j);
        }
    }


    cout <<endl<<"Solucion encontrada por el metodo de Gauss"<<endl;
    prueba = new Gauss();
    prueba->calcular(*m);

    float *solucion;
    solucion=prueba->getSolucion();

    for(int i=0;i<a;i++)
        cout <<endl<< "solucion["<<i<<"]: "<<solucion[i ]<<endl;



    cout <<endl<<"Solucion encontrada por el metodo de Cramer"<<endl;
    prueba = new Cramer();
    prueba->calcular(*m);

    solucion=prueba->getSolucion();

    for(int i=0;i<a;i++)
        cout <<endl<< "solucion["<<i<<"]: "<<solucion[i ]<<endl;

    return 0;
}*/
