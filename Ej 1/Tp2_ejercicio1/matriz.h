#ifndef MATRIZ_H
#define MATRIZ_H


class Matriz
{
private:
    int cantF;
    int cantC;
    float **m;
    float *solucion;
public:
    Matriz(int,int);
    float* getSol();
    int getCantC();
    int getCantF();
    void setVal(float,int,int);
    float getVal(int,int);
    void mostrar();
    ~Matriz();

};

#endif // MATRIZ_H
