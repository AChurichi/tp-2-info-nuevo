#include "gauss.h"


Gauss::Gauss()
{

}

void Gauss::calcular(Matriz mat)
{

    m=mat.getCantF();//guardo las filas
    n=mat.getCantC();//guardo las columnas


    if(mat.getCantC()-mat.getCantF()<1)//si el sistema esta mal dimensionado no lo resuelvo
    {
        cout << "Infinitas soluciones" << endl;

    }
    else if(mat.getCantC()-mat.getCantF()==1)/* Solo resuelve matrices en las que hay una columna mas que la cantidad de filas*/
    {
        setAux(mat);//cargo los valores

        /***************************************************************************************************/
        triSup();
        /*formando la matriz escalonada */

//        mostrar();


        /*Ahora se supone que hago la sustitución hacia atrás*/
        sustitucionRegresiva();


//        for(int i=0;i<m;i++)
//            cout <<endl<< "solucion["<<i<<"]: "<<solucion[i ]<<endl;



    }



}


void Gauss::triSup()
{
    for(int i=0; i<m;i++)
    {
        float pivot=this->aux[i][i];// este pivot es para dividir todo el renglon por el, y formar el pivote de ese renglon

        for(int j=i; j<n ; j++)
        {

            if(aux[i][i]==0)
            {
//                cout << "ATENCION:::: iNTERCAMBIANDO RENGLONES"<<endl;
                intercambiarRenglon(i,i+1);
                pivot=this->aux[i][i];//pivote


            }
            if(aux[i][i]!=0)
            {
                aux[i][j]=aux[i][j]*(1/pivot);//divido toda la columna por el pivote



            }
//            mostrar();
        }

        /*En esta parte del codigo, se arman 0 en todas las posiciones debajo del pivot*/
        for(int h=i+1; h<m ; h++)

        {
            float pivot2 = aux[h][i];
//            mostrar();


            for(int j=i; j<n; j++)
            {

                aux[h][j]=aux[h][j]-aux[i][j]*pivot2;
//                mostrar();

            }

        }
    }
//    mostrar();

}

void Gauss::sustitucionRegresiva()/*Buscar teorema  de la sustitucion regresiva
                                    o bien Algoritmo del metodo de sustitucion regresiva*/

{
    int incognitas;

    incognitas=this->n-1;

    this->solucion=new float[incognitas];

    for(int i=incognitas-1; i>=0 ; i--)

    {
        int j=i+1;

        solucion[i]=(aux[i][n-1]-sumatoria(m-j));
    }
}

float Gauss::sumatoria(int cant)//para la sustitucion regresiva
{
    float suma=0;
    int indice=n-2;
    for(int i=indice;i>indice-cant;i--)
    {

        suma=suma+(solucion[i]*aux[indice-cant][i]);
    }

    return suma;

}

void Gauss::intercambiarRenglon(int r1,int r2)//doy vuelta los valores uno por uno, no me salio mover los punteros
{
    float auxiliar;
    for(int i=0;i<n;i++)
    {
        auxiliar=aux[r1][i];
        aux[r1][i]=aux[r2][i];
        aux[r2][i]=auxiliar;
//        mostrar();
    }
}
