#include "matriz.h"
#include <iostream>

using namespace std;

Matriz::Matriz(int F,int C)
{
    this->cantF=F;
    this->cantC=C;
    m= new float*[F];
    for(int i=0;i<F;i++)
        m[i]=new float[C];
    if(!m)
        cout << "no se pudo reservar el espacio de memoria" << endl;
    for (int i=0;i<getCantF();i++)
    {
        for(int j=0;j<getCantC();j++)//lleno la matriz con 0
        {
            setVal(0,i,j);
        }
    }
}

int Matriz::getCantC()
{
    return this->cantC;
}

int Matriz::getCantF()
{
    return this->cantF;
}

float*Matriz::getSol()
{
    return this->solucion;
}

float Matriz::getVal(int i,int j)
{
    return this->m[i][j];
}

void Matriz::setVal(float val, int i, int j)
{
    if(i<getCantF()&&j<getCantC())
    {
        this->m[i][j]=val;
    }
    else
        cout << "Error: Imposible insertar el valor" << endl;
}

Matriz::~Matriz()//falta escribir esto
{

//        delete[] m;



}

void Matriz::mostrar()
{
    cout << endl << "Matriz" << endl;
    for (int i=0;i<getCantF();i++)
    {

        for(int j=0;j<getCantC();j++)
        {
            cout << getVal(i,j) << " ";
        }
        cout << endl;
    }
}

