#include "calabozo.h"
#include <ctime>
#include <cstdlib>

Calabozo::Calabozo(){
    //Generamos el calabozo en un casillero aleatorio entre el 1 y 49
    this->pos = (rand() % 49) + 1;
}

int Calabozo::getPos(){ return this->pos; }
