#include <iostream>
#include "juego.h"

using namespace std;

int main()
{
    cout << "BIENVENIDO A: CABALLEROS, CALABOZOS Y DRAGONES" << endl;
    cout << endl << "En que nivel de dificultad vas a jugar?" << endl;
    int op1, op2;
    do{
        cout << "Basisco (1)" << endl <<
                "Intermedio (2)" << endl <<
                "Experto (3)" << endl <<
                "Salir (4)" << endl;
        cin >> op1;
    }while(op1 < 1 && op1 > 4);
    if(op1 == 4)
        return 0;
    Juego elJuego(op1);

    do{
        do{
            cout << "Desea vovler a jugar? Si(1) / No(2)" << endl;
            cin >> op1;
        }while(op1 != 1 && op1 != 2);
        if(op1 == 1){
            system("clear");
            do{
                cout << "Nivel de Dificultad?" << endl <<
                        "Basico (1)" << endl <<
                        "Intermedio (2)" << endl <<
                        "Experto (3)" << endl;
                cin >> op2;
            }while(op2 != 1 && op2 != 2 && op2 != 3);
            elJuego.reiniciar(op2);
        }
        else
            cout << endl << "Gracias por jugar!" << endl << endl;
    }while(op1 != 2);
    return 0;
}
