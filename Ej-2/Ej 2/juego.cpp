#include "juego.h"
#include <iostream>
#include "basico.h"
#include "intermedio.h"
#include "experto.h"
/*Ver si es necesario include de "dado.h"*/

using namespace std;

Juego::Juego(int dificultad){
    this->ronda = 0;
    this->termino = false;
    switch(dificultad){
    case 1:
        this->nivelJuego = new basico;
        break;
    case 2:
        this->nivelJuego = new Intermedio;
        break;
    case 3:
        this->nivelJuego = new Experto;
        break;
    }
    jugar();
}

void Juego::jugar(){
    int estadoJuego;
    this->ronda++;
    cout << endl << "--------- Ronda: " << this->ronda << " ---------" << endl;
    estadoJuego = nivelJuego->avanzarRonda(elDado);
    if(estadoJuego == 1)
        finDelJuego(1);
    else if(estadoJuego == 2)
        finDelJuego(2);
    else
        jugar();
}

int Juego::finDelJuego(int ganador){
    /* Si el valor ingresado como parámetro es 1 el ganador es el jugador real, si es
     * 2 el ganador es el jugador virtual, se imprime por pantalla el texto
     * correspondiente y luego se pregunta al jugador si desea volver a jugar
     * Se retorna el valor 1 en caso de que el juego haya terminado
     * Se retorna el valor 0 en caso de que el juego NO haya terminado
     */
    if(ganador == 1 || ganador == 2){
        cout << endl << "-----------------------------" << endl;
        if(ganador == 1)
            cout << endl << "EL JUEGO TERMINO: GANASTE!" << endl << endl;
        else
            cout << endl << "EL JUEGO TERMINO: PERDISTE!" << endl << endl;
        this->termino = true;
        delete nivelJuego;
        return 1;
    }
    else
        return 0;
}

void Juego::reiniciar(int dificultad){
    if(this->termino == true){
        this->termino = false;
        this->ronda = 0;
        switch(dificultad){
        case 1:
            this->nivelJuego = new basico;
            break;
        case 2:
            this->nivelJuego = new Intermedio;
            break;
        case 3:
            this->nivelJuego = new Experto;
            break;
        }
        jugar();
    }
    else
        cout << "Error: El juego aun no ha terminado" << endl;
}

bool Juego::getTermino(){ return this->termino; }
