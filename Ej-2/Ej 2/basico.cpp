#include "basico.h"
#include <iostream>

using namespace std;

basico::basico()
{
    this->jugadores=new jugador[2];
    jugadores[0].setReal(true);
    jugadores[1].setReal(false);

}

int basico::avanzarRonda(Dado elDado){
    /* Método ncargado de avanzar la ronda
     * Retorna 0 si no ganó nadie
     * Retorna 1 si ganó el jugador 1
     * Retorna 2 si ganó el jugador 2
     */
    for(int i=0; i<2; i++){ //For para mover a los jugadores
        int moverse = elDado.tirar();
        jugadores[i].moverse(moverse);
        if(jugadores[i].esReal() == true){ //Si se movio tu jugador
            cout << endl << "Te has movido " << moverse << " casilla/s" << endl;
            cout << "Ahora estas en el casillero " << jugadores[i].getPos() << endl;
            if(jugadores[i].getPos() >= 50)
                return 1;
        }
        else{ //Si se movio el jugador enemigo
            cout << endl << "El jugador enemigo se movio " << moverse << " casilla/s" << endl;
            cout << "Ahora esta en el casillero " << jugadores[i].getPos() << endl;
            if(jugadores[i].getPos() >= 50)
                return 2;
        }
    }
    return 0;
}

basico::~basico()
{
        delete []jugadores;
}
