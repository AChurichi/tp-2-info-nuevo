#include "intermedio.h"
#include <iostream>

using namespace std;

Intermedio::Intermedio() : basico()
{
    dragones = new Dragon[4];
    for(int i=0;i<2;i++)
        dragones[i].setReal(true);
    for(int i=2;i<4;i++)
        dragones[i].setReal(false);
}

Dragon* Intermedio::getDragones(){ return this->dragones; }

int Intermedio::avanzarRonda(Dado elDado){
    /* Método ncargado de avanzar la ronda
     * Retorna 0 si no ganó nadie
     * Retorna 1 si ganó el jugador 1
     * Retorna 2 si ganó el jugador 2
     */
    for(int i=0; i<4; i++) //For para mover a los dragones
        dragones[i].moverse();
    for(int i=0; i<2; i++){ //For para mover a los jugadores
        int moverse = elDado.tirar();
        jugadores[i].moverse(moverse);
        if(jugadores[i].esReal() == true){ //Si se movio tu jugador
            cout << endl << "Te has movido " << moverse << " casilla/s" << endl;
            cout << "Ahora estas en el casillero " << jugadores[i].getPos() << endl;
            jugadorDragon(i); //Llamamos al metodo que controla si un jugador se encontró con un dragón
            if(jugadores[i].getPos() >= 50)
                return 1;
        }
        else{ //Si se movio el jugador enemigo
            cout << endl << "El jugador enemigo se movio " << moverse << " casilla/s" << endl;
            cout << "Ahora esta en el casillero " << jugadores[i].getPos() << endl;
            jugadorDragon(i); //Llamamos al metodo que controla si un jugador se encontró con un dragón
            if(jugadores[i].getPos() >= 50)
                return 2;
        }
    }
    return 0;
}

void Intermedio::jugadorDragon(int numJugador){
    /* Si un jugador cae donde está un dragón se deben tomar las medidas correspondientes
     * dependiendo de cuantos dragones haya en el casillero */
    int dPropio = 0, dEnemigo = 0; //Para contar en caso que haya mas de un dragón en un casillero
    for(int j=0; j<4; j++){ //For para los dragones
        if(jugadores[numJugador].getPos() == dragones[j].getPos()){ //Si hay un jugador en la posicion de un dragon
                    if(jugadores[numJugador].esReal() == dragones[j].esReal()) //Si el dragón pertenece al jugador
                        dPropio++;
                    else //Si pertenece al jugador enemigo
                        dEnemigo++;
        }
    }
    if(dPropio != 0 || dEnemigo != 0){ //Hay uno o mas dragones en la posición de un jugador
        if(dPropio == 0)
            cout << "En la casilla habia " << dEnemigo << " dragon/es enemigo/s, ";
        else if(dEnemigo == 0)
            cout << "En la casilla habia " << dPropio << " dragon/es aliado/s, ";
        else
            cout << "En la casilla habia " << dPropio << " dragon/es aliado/s y " << dEnemigo << " dragon/es enemigo/s, ";
        switch(dPropio - dEnemigo){
            case 2: //Hay dos dragones del jugador
                jugadores[numJugador].moverse(10);
                cout << "avanza 10 casilleros" << endl;
                break;
            case 1: //Hay un dragon del jugador
                jugadores[numJugador].moverse(5);
                cout << "avanza 5 casilleros" << endl;
                break;
            case -1: //Hay un dragon enemigo
                jugadores[numJugador].moverse(-5);
                cout << "retrocede 5 casilleros" << endl;
                break;
            case -2: //Hay dos dragones enemigos
                jugadores[numJugador].moverse(-10);
                cout << "retrocede 10 casilleros" << endl;
                break;
        }
        cout << "Ahora esta en el casillero " << jugadores[numJugador].getPos() << endl;
    }
}

Intermedio::~Intermedio(){ delete []dragones; }
