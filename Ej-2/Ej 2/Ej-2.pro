TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    experto.cpp \
    calabozo.cpp \
    juego.cpp \
    basico.cpp \
    jugador.cpp \
    intermedio.cpp \
    dragon.cpp \
    dado.cpp

HEADERS += \
    experto.h \
    calabozo.h \
    juego.h \
    basico.h \
    jugador.h \
    intermedio.h \
    dragon.h \
    dado.h
