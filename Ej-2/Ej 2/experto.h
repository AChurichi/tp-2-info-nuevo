#ifndef EXPERTO_H
#define EXPERTO_H
#include "intermedio.h"
#include "calabozo.h"
#include "dragon.h"

/* Esta clase define el modo de juego experto. Desde esta clase se crea el vector de
 * calabozos.
 */

class Experto : public Intermedio
{
private:
    Calabozo *calabozos;
public:
    Experto();
    Calabozo* getCalabozos();
    virtual int avanzarRonda(Dado);
    int jugadorCalabozo(int, bool *);
    virtual ~Experto();
};

#endif // EXPERTO_H
