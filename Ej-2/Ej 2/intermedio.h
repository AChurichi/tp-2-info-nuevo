#ifndef INTERMEDIO_H
#define INTERMEDIO_H
#include "basico.h"
#include "dragon.h"

class Intermedio: public basico
{
protected:
    Dragon *dragones;
public:
    Intermedio();
    Dragon* getDragones();
    virtual int avanzarRonda(Dado);
    void jugadorDragon(int);
    virtual ~Intermedio();
};

#endif // INTERMEDIO_H
