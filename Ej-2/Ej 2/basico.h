#ifndef BASICO_H
#define BASICO_H
#include "jugador.h"
#include "dado.h"

class basico
{
protected:
    jugador *jugadores;
public:
    basico();
    virtual int avanzarRonda(Dado);
    virtual ~basico();
};

#endif // BASICO_H
