#ifndef DADO_H
#define DADO_H

// Clase referente al dado, este será el que nos dirá cuantos casilleros avanzar

class Dado
{  
public:
    Dado();
    int tirar();
};

#endif // DADO_H
