#ifndef CALABOZO_H
#define CALABOZO_H

/* Clase referente a los calabozos, en el constructor se les asigna una posición.
 * tienen el método "caeJugador" para poder llevar a cabo la acción necesaria en
 * caso de que un jugador caiga en un casillero donde haya un calabozo.
 */

class Calabozo
{
private:
    int pos;
public:
    Calabozo();
    int getPos();
};

#endif // CALABOZO_H
