#include "dragon.h"
#include <ctime>
#include <cstdlib>



Dragon::Dragon(){ this->pos = (rand() % 50) + 1; }

void Dragon::setReal(bool real){
    this->real = real;
}

void Dragon::moverse(){
    /* Los dragones pueden moverse una casilla a la izquierda o a la derecha de su
     * posicion actual */
    if(this->pos == 1) //En el caso de que se encuentre en el primer casillero debe ir si o si al segundo
        this->pos++;
    else if(this->pos == 50) //En el caso que se encuentre en el casillero 50 debe ir si o si al casillero 49
        this->pos--;
    else{ //Si no se encuentra en ninguna de las posiciones anteriores se mueve aleatoriamente
        if((rand() % 2+1) == 1) //Se genera un numero aleatorio, si es 1 el dragón se mueve a la derecha
            this->pos++;
        else //Sino el resultado sera 2, el dragón se mueve hacia la izquierda
            this->pos--;
    }
}

int Dragon::getPos(){ return this->pos; }

bool Dragon::esReal(){ return this->real; }
