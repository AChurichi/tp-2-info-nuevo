#include "experto.h"
#include <iostream>
//ver si no hay que poner #include "calabozo.h"

using namespace std;

Experto::Experto() : Intermedio()
{
    this->calabozos = new Calabozo[3];
}

Calabozo* Experto::getCalabozos(){ return this->calabozos; }

int Experto::avanzarRonda(Dado elDado){
    /* Método encargado de avanzar la ronda
     * Retorna 0 si no ganó nadie
     * Retorna 1 si ganó el jugador 1
     * Retorna 2 si ganó el jugador 2
     */
    bool hayDragones;
    for(int i=0; i<4; i++) //For para mover a los dragones
        dragones[i].moverse();
    for(int i=0; i<2; i++){ //For para mover a los jugadores
        hayDragones = false;
        if(jugadores[i].getPuedeJugar() == true){
            int moverse = elDado.tirar();
            jugadores[i].moverse(moverse);
            if(jugadores[i].esReal() == true){ //Si se movio tu jugador
                cout << endl << "Te has movido " << moverse << " casilla/s" << endl;
                cout << "Ahora estas en el casillero " << jugadores[i].getPos() << endl;
                if(jugadorCalabozo(i, &hayDragones) == 1) //Llamamos al método para ver si el jugador se encontró con un calabozo
                    return 2; //Si entra en el if, significa que el jugador perdio
                if(hayDragones == false) //Si habia dragones y calabozos el metodo anterior cambia esta variable a ture
                    jugadorDragon(i); //Llamamos al metodo que controla si un jugador se encontró con un dragón
                if(jugadores[i].getPos() >= 50)
                    return 1;
            }
            else{ //Si se movio el jugador enemigo
                cout << endl << "El jugador enemigo se movio " << moverse << " casilla/s" << endl;
                cout << "Ahora esta en el casillero " << jugadores[i].getPos() << endl;
                if(jugadorCalabozo(i, &hayDragones) == 1) //Llamamos al método para ver si el jugador se encontró con un calabozo
                    return 1; //Si entra en el if, significa que el jugador perdio
                if(hayDragones == false) //Si habia dragones y calabozos el metodo anterior cambia esta variable a ture
                    jugadorDragon(i); //Llamamos al metodo que controla si un jugador se encontró con un dragón
                if(jugadores[i].getPos() >= 50)
                    return 2;
            }
        }
        else{
            cout << endl << "El jugador en el calabozo pierde este turno" << endl;
            jugadores[i].setPuedeJugar(true);
        }
    }
    return 0;
}

int Experto::jugadorCalabozo(int numJugador, bool *hayDragones){
    /*Este método será el encargado de revisar en cada turno si un jugador se
      encontro con un calabozo, también se verá si hay dragones en ellos para
      así llevar a cabo las acciones correspondientes*/
    /* Devuelve 1 si el jugador perdio, de lo contrario devuelve 0 */

    int dPropio, dEnemigo; //Estos contadores nos ayudaran a saber cuantos dragones tenemos en el calabozo
    bool hayCalabozo; //Bandera que nos dirá si hay jugadores en el calabozo
    for(int j=0; j<3; j++){ //For para los calabozos
        hayCalabozo = false; // por defecto es false
        dPropio = 0; //Seteamos las variables en 0
        dEnemigo = 0;
        if(jugadores[numJugador].getPos() == calabozos[j].getPos()){ //Si hay un jugador en un calabozo
            hayCalabozo = true;
            for(int k=0; k<4; k++){ //For para los dragones
                if(jugadores[numJugador].getPos() == dragones[k].getPos()){ //Si también hay dragones
                    if(jugadores[numJugador].esReal() == dragones[k].esReal()) //Si el dragón pertenece al jugador
                        dPropio++;
                    else //Si pertenece al jugador enemigo
                        dEnemigo++;
                }
            }
        }
        if(hayCalabozo == true){ //Si el jugador cayó en un calabozo
            if((dPropio == 0 && dEnemigo == 0) || (dPropio - dEnemigo == 0)){ //No habia dragones en el calabozo o los habia en igual cantidad y se cancelan
                jugadores[numJugador].setPuedeJugar(false); //El jugador debe perder un turno
                cout << "El jugador cayo en un calabozo, pierde un turno!" << endl;
            }
            else{
                *hayDragones = true;
                if(dPropio == 0)
                    cout << "El jugador cayo en un calabozo y habia " << dEnemigo << " dragon/es enemigo/s, ";
                else if(dEnemigo == 0)
                    cout << "El jugador cayo en un calabozo y habia " << dPropio << " dragon/es aliado/s, ";
                else
                    cout << "El jugador cayo en un calabozo y habia  " << dPropio << " dragon/es aliado/s y " << dEnemigo << " dragon/es enemigo/s, ";
                switch(dPropio - dEnemigo){
                    case 2: //Hay dos dragones propios pero un calabozo
                        jugadores[numJugador].moverse(5);
                        cout << "avanza 5 casilleros" << endl;
                        break;
                    case 1: //Hay un dragon propio y un calabozo
                        cout << "no sucede nada" << endl;
                        break;
                    case -1: //Hay un dragon enemigo y un calabozo
                        cout << "el jugador perdio!" << endl;
                        return 1;
                        break;
                    case -2: //Hay dos dragones enemigos y un calabozo
                        cout << "el jugador perdio!" << endl;
                        return 1;
                        break;
                }
            }
        }
    }
    return 0;
}

Experto::~Experto(){ delete []calabozos; }
