#ifndef DRAGON_H
#define DRAGON_H


class Dragon
{
private:
    int pos;
    bool real;
public:
    Dragon();
    void setReal(bool);
    void moverse();
    int getPos();
    bool esReal();
};

#endif // DRAGON_H
