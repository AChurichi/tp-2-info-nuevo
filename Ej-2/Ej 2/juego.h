#ifndef JUEGO_H
#define JUEGO_H
#include "dado.h"
#include "basico.h"
#include "intermedio.h"
#include "experto.h"
#include "dragon.h"
#include "calabozo.h"

/* Clase principal que engloba a todas las otras. En ella se deciden cuestiones
 * fundamentales del juego, como decidir quien gano, el número de rondas que se van
 * jugando o la creación del dado.
 */

class Juego
{
private:
    Dado elDado;
    int ronda;
    bool termino;
    basico *nivelJuego;
public:
    Juego(int);
    void jugar(void);
    int finDelJuego(int);
    void reiniciar(int);
    bool getTermino();
};

#endif // JUEGO_H
