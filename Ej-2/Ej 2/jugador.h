#ifndef JUGADOR_H
#define JUGADOR_H


class jugador
{
private:
    int pos;//posicion
    bool real;//humano o pc
    bool puedeJugar;
public:
    jugador();
    void setReal(bool);//metodo para decir si es humano o pc
    void moverse(int);//moverse
    void setPos(int);//fijar posicion
    int getPos();//pedir posicion
    bool esReal();//comprobar si es humano o pc
    void setPuedeJugar(bool);
    bool getPuedeJugar();
};

#endif // JUGADOR_H
