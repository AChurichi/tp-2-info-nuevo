#include "jugador.h"

jugador::jugador()
{
    this->puedeJugar = true;
    setPos(1);
}

void jugador::setReal(bool real)
{
    this->real=real;
}

bool jugador::esReal()
{
    return real;
}

void jugador::setPos(int pos)
{
    this->pos=pos;
}

int jugador::getPos()
{
    return this->pos;
}

void jugador::moverse(int movimiento)/*puro encapsulamiento innecesario*/
{
    int posicion;
    posicion=getPos();
    setPos(posicion+movimiento);
    if(pos<=1)
        pos=1;
    else if(pos>=50)
        pos=50;
}

void jugador::setPuedeJugar(bool estado)
{
    this->puedeJugar = estado;
}

bool jugador::getPuedeJugar()
{
    return this->puedeJugar;
}
